const Product = require('../models/product')
const Order = require('../models/order')
const Admin = require('../models/admin')
const bcrypt = require('bcryptjs')

class AdminController {
    async getProducts(req, res) {
        const products = await Product.getProducts()

        res.render('admin/admin-panel', {
            layout: 'admin',
            title: 'Admin panel',
            products
        })
    }
    async addProduct(req, res) {
        Product.addProduct(req.body)
        res.redirect('/admin')
    }
    async editProduct(req, res) {
        Product.editProduct(req.body)
        res.redirect('/admin')
    }
    async deleteProduct(req, res) {
        Product.deleteProduct(req.body.id)
        res.redirect('/admin')
    }
    async getOrders(req, res) {
        const orders = await Order.getOrders()

        res.render('admin/admin-orders', {
            layout: 'admin',
            title: 'Admin panel',
            orders
        })
    }
    async doneOrder(req, res) {
        const status = await Order.doneOrder(req.body.id)

        res.status(200).json(true)
    }
    async loginPage(req, res) {
        res.render('admin/admin-auth', {
            layout: 'empty',
            title: 'Авторизация'
        })
    }
    async login(req, res) {
        try {
            const {login, password} = req.body
            const candidate = await Admin.findOne(login)
            
            if (candidate) {
                const areSame = await bcrypt.compare(password, candidate.password_hash)
    
                if (areSame) {
                    req.session.isStaffAuthenticated = true
                    if (candidate.position_staff === 'admin') {
                        req.session.isAdmin = true
                    }
                    req.session.save(err => {
                        if (err) {
                            throw err
                        }
                        res.redirect('/admin')
                    })
                } else {
                    res.redirect('/admin/auth')
                }
    
            } else {
                res.redirect('/admin/auth')
            }
    
        } catch (e) {
            console.log(e)
        }
    }
    async register(req, res) {
        try {
            const { login, password, position } = req.body
            const password_hash = await bcrypt.hash(password, 10)

            const status = await Admin.registration({
                login , password_hash, position
            })

            res.redirect('/admin/staff')
        } catch (error) {
            throw error
        }
    }
    async staffPage(req, res) {
        const staff = await Admin.getAll()

        res.render('admin/admin-staff', {
            layout: 'admin',
            title: 'Admin panel',
            staff
        })
    }
    async staffDelete(req, res){
        Admin.deleteStaff(req.body.id)
        res.redirect('/admin/staff')
    }
}

module.exports = new AdminController()