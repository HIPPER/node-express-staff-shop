const User = require('../models/user')
const bcrypt = require('bcryptjs')

class UserController {
    async authPage(req, res) {
        res.render('auth', {
            title: 'Staff - бренд качественной молодежной одежды.',
            error: req.flash('error')
        })
    }
    async regPage(req, res) {
        res.render('registration', {
            title: 'Staff - бренд качественной молодежной одежды.',
            error: req.flash('error')
        })
    }
    async login(req, res) {
        try {
            const {phone, password} = req.body
            const candidate = await User.findOne(phone)

            if (candidate) {
                const areSame = await bcrypt.compare(password, candidate.password_hash)
                
                if (areSame) {
                    UserController.auth(req, res, candidate)
                } else {
                    req.flash('error', 'Некорректные данные!')
                    res.redirect('/auth')
                }
            } else {
                req.flash('error', 'Некорректные данные!')
                res.redirect('/auth')
            }

        } catch (error) {
            console.log(error)
        }
    }
    async logout(req, res) {
        req.session.destroy(() => {
            res.redirect('/auth')
        })
    }
    static async auth(req, res, candidate) {
        req.session.user = candidate
        req.session.isAuthenticated = true
        req.session.save(err => {
            if (err) {
                throw err
            }
            res.redirect('/')
        })
    }
    async registration(req, res) {
        try {
            const {phone, full_name, password, password_repeat} = req.body
            if (password !== password_repeat) {
                req.flash('error', 'Пароли не совпадают!')
                res.redirect('/auth/registration')
            }

            const candidate = await User.findOne(phone)
            if (!candidate) {
                const hashPassword = await bcrypt.hash(password, 10)
                await User.registration({
                    phone, full_name, password: hashPassword
                })
                UserController.auth(req, res, candidate)
            } else {
                req.flash('error', 'Пользователь с таким номером уже существует!')
                res.redirect('/auth/registration')
            }
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = new UserController()