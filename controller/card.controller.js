const Product = require('../models/product')

class CardController {
    static computePrice(products) {
        return products.reduce((total, product) => {
            return total += product.price * product.count
        }, 0)
    }
    async getCard(req, res) {
        if (!req.session.cart) {
            res.render('card', {
                title: 'Корзина',
                price: 0
            })
        } else {
            const products = req.session.cart

            res.render('card', {
                title: 'Корзина',
                price: CardController.computePrice(products),
                user: req.session.user,
                products
            })
        }    
    }
    async addCardItem(req, res) {
        let obj = {
            id: req.body.id,
            title: req.body.title,
            price: req.body.price,
            img: req.body.img,
            size: req.body.size,
            count: 1
        }
        if (!req.session.cart) {
            req.session.cart = [obj]
        } else {
            let items = [...req.session.cart]
            const idx = items.findIndex(c => {
                return c.id.toString() === req.body.id.toString()
            })
            
            if (idx < 0) {
                items.push(obj)
            }
            req.session.cart = items
        }
    
        req.session.save(err => {
            if (err) {
                throw err
            }
    
            res.sendStatus(200)
        })
    }
    async changeQuantity(req, res) {
        let items = [...req.session.cart]
        const idx = items.findIndex(c => {
            return c.id.toString() === req.body.id.toString()
        })

        items[idx].count = req.body.count
        req.session.cart = items
        req.session.save(err => {
            if (err) {
                throw err
            }

            res.status(200).json(CardController.computePrice(req.session.cart))
        })
    }
    async removeCardItem(req, res) {
        const itemsCart = [...req.session.cart]
        const id = itemsCart.findIndex(c => {
            return c.id.toString() === req.params.id.toString()
        })
        const items = itemsCart.filter(c => c.id !== req.params.id)
        req.session.cart = items

        res.status(200).json(req.session.cart)
    }
}

module.exports = new CardController()