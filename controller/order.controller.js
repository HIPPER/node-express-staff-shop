const Order = require('../models/order')

class OrderController {
    async addOrder(req, res) {
        const order = {
            form: req.body,
            products: req.session.cart
        }
        const status = await Order.createOrder(order)
        delete req.session.cart
        res.redirect('/')
    }
}

module.exports = new OrderController()