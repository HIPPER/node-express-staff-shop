class HomeController {
    async show(req, res) {
        res.render('index', {
            title: 'Staff - бренд качественной молодежной одежды.'
        })
    }
}

module.exports = new HomeController()