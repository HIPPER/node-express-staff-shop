const Product = require('../models/product')
const Reviews = require('../models/review')

class ProductController {
    async getAllMale(req, res) {
        const products = await Product.getAllMale()

        res.render('products', {
            title: 'Staff - бренд качественной молодежной одежды.',
            isParnyam: true,
            products
        })
    }
    async getProductsOnCategoryMale(req, res) {
        const products = await Product.getProductsOnCategory('M', req.params.category)

        res.render('products', {
            title: 'Staff - бренд качественной молодежной одежды.',
            isParnyam: true,
            products
        })
    }
    async getAllFemale(req, res) {
        const products = await Product.getAllFemale()

        res.render('products', {
            title: 'Staff - бренд качественной молодежной одежды.',
            isDevushkam: true,
            products
        })
    }
    async getProductsOnCategoryFemale(req, res) {
        const products = await Product.getProductsOnCategory('F', req.params.category)

        res.render('products', {
            title: 'Staff - бренд качественной молодежной одежды.',
            isDevushkam: true,
            products
        })
    }
    async getById(req, res) {
        const product = await Product.getById(req.params.id)
        const reviews = await ProductController.getReviews(req.params.id)
        
        res.render('product', {
            title: `${product.title}`,
            error: req.flash('error'),
            product,
            reviews
        })
    }
    async find(req, res) {
        const favourites = await Product.find(req.session.idFavourites)

        res.render('favourites', {
            title: 'Избранное',
            favourites
        })
    }
    async addFavourite(req, res) {
        if (!req.session.idFavourites) {
            req.session.idFavourites = [req.body.id]
        } else {
            const items = [...req.session.idFavourites]
            const idx = items.findIndex(c => {
                return c.toString() === req.body.id.toString()
            })
    
            if (idx < 0) {
                items.push(req.body.id)
            }
    
            req.session.idFavourites = items
        }
    
        req.session.save(err => {
            if (err) {
                throw err
            }
    
            res.sendStatus(200)
        })
    }
    async deleteFavourite(req, res) {
        const idx = req.session.idFavourites.findIndex(c => {
            return c.toString() === req.body.id.toString()
        })
    
        req.session.idFavourites.splice(idx, 1)
        res.sendStatus(200)
    }
    static async getReviews(id) {
        const reviews = await Reviews.getById(id)

        return reviews
    }
    async addReview(req, res) {
        try {
            const status = await Reviews.add(req.session.user.id, req.body.id, req.body.comment)
            if (status.original.code === 'P0001') {
                req.flash('error', 'Пользователь не может добавить более одного комментария на товар!')
            }
            res.redirect(`/product/${req.body.id}`)
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = new ProductController()