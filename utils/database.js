const { Sequelize } = require('sequelize')
const keys = require('../keys')

const sequelize = new Sequelize(keys.DATABASE_CONFIG, {
    logging: false
})

module.exports = sequelize