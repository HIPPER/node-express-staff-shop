module.exports = {
    MONGODB_URI: process.env.MONGODB_URI,
    SESSION_SECRET: process.env.SESSION_SECRET,
    DATABASE_CONFIG: process.env. DATABASE_CONFIG
}