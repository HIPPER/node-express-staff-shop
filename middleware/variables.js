module.exports = function(req, res, next) {
    res.locals.isAuth = req.session.isAuthenticated
    res.locals.isStaffAuth = req.session.isStaffAuthenticated
    res.locals.isAdmin = req.session.isAdmin

    next()
}