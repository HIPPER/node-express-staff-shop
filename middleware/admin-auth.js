module.exports = function(req, res, next) {
    if (!req.session.isStaffAuthenticated) {
        res.redirect('/admin/auth')
    }

    next()
}