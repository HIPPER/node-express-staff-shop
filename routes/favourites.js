const {Router} = require('express')
const Product = require('../models/product')
const productController = require('../controller/product.controller')
const router = Router()

router.get('/', productController.find)

router.post('/', productController.addFavourite)
/*
router.post('/', (req, res) => {
    if (!req.session.idFavourites) {
        req.session.idFavourites = [req.body.id]
    } else {
        const items = [...req.session.idFavourites]
        const idx = items.findIndex(c => {
            return c.toString() === req.body.id.toString()
        })

        if (idx < 0) {
            items.push(req.body.id)
        }

        req.session.idFavourites = items
    }

    req.session.save(err => {
        if (err) {
            throw err
        }

        res.sendStatus(200)
    })
})
*/

router.post('/delete', productController.deleteFavourite)

/*
router.post('/delete', (req, res) => {

    const idx = req.session.idFavourites.findIndex(c => {
        return c.toString() === req.body.id.toString()
    })

    req.session.idFavourites.splice(idx, 1)
    res.sendStatus(200)
})
*/

module.exports = router 