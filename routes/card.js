const {Router} = require('express')
const cardController = require('../controller/card.controller')
const router = Router()

// Get all items of card
router.get('/', cardController.getCard)

// Set item of card
router.post('/', cardController.addCardItem)

// Change quantity of item
router.post('/quantity', cardController.changeQuantity)

// Remove item of card
router.post('/remove/:id', cardController.removeCardItem)

/*
router.post('/', (req, res) => {
    if (!req.session.idProducts) {
        req.session.idProducts = [req.body.id]
    } else {
        const items = [...req.session.idProducts]
        const idx = items.findIndex(c => {
            return c.toString() === req.body.id.toString()
        })

        if (idx < 0) {
            items.push(req.body.id)
        }

        req.session.idProducts = items
    }

    req.session.save(err => {
        if (err) {
            throw err
        }

        res.sendStatus(200)
    })
})
*/

module.exports = router