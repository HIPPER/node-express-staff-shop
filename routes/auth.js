const {Router} = require('express')
const userController = require('../controller/user.controller')
const router = Router()

router.get('/', userController.authPage)

router.get('/registration', userController.regPage)

router.post('/', userController.login)

router.post('/registration', userController.registration)

router.get('/logout', userController.logout)


module.exports = router