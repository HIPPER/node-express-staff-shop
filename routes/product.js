const {Router} = require('express')
const productController = require('../controller/product.controller')
const router = Router()

router.get('/:id', productController.getById)
router.post('/review', productController.addReview)

module.exports = router