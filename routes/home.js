const {Router} = require('express')
const homeController = require('../controller/home.controller')
const router = Router()

router.get('/', homeController.show)

module.exports = router