const {Router} = require('express')
const productController = require('../controller/product.controller')
const router = Router()

router.get('/', productController.getAll)

module.exports = router