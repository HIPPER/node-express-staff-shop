const {Router} = require('express')
const productController = require('../controller/product.controller')
const router = Router()

router.get('/parnyam', productController.getAllMale)
router.get('/parnyam/:category', productController.getProductsOnCategoryMale)

router.get('/devushkam', productController.getAllFemale)
router.get('/devushkam/:category', productController.getProductsOnCategoryFemale)


module.exports = router