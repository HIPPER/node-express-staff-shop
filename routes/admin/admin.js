const {Router} = require('express')
const adminController = require('../../controller/admin.controller')
const auth = require('../../middleware/admin-auth')
const router = Router()

router.get('/', auth, adminController.getProducts)

router.post('/add', auth, adminController.addProduct)

router.post('/edit', auth, adminController.editProduct)

router.post('/delete', auth, adminController.deleteProduct)

router.get('/order', auth, adminController.getOrders)

router.post('/order', auth, adminController.doneOrder)

router.get('/staff', auth, adminController.staffPage)

router.post('/staff', auth, adminController.register)

router.post('/staff/delete', auth, adminController.staffDelete)

module.exports = router