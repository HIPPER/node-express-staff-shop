const {Router} = require('express')
const bcrypt = require('bcryptjs')
const Admin = require('../../models/admin')
const adminController = require('../../controller/admin.controller')
const router = Router()

router.get('/', adminController.loginPage)

router.post('/', adminController.login)

module.exports = router