const {Router} = require('express')
const orderController = require('../controller/order.controller')
const router = Router()

router.post('/', orderController.addOrder)

module.exports = router