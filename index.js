const express = require('express')
const path = require('path')
const flash = require('connect-flash')
const exphbs = require('express-handlebars')
const sequelize = require('./utils/database')
const session = require('express-session')
const SessionStore = require('express-session-sequelize')(session.Store)
const varMiddleware = require('./middleware/variables')

const homeRoutes = require('./routes/home')
const productsRoutes = require('./routes/products')
const productRoutes = require('./routes/product')
const favouritesRoutes = require('./routes/favourites')
const cardRoutes = require('./routes/card')
const orderRoutes = require('./routes/order')
const authRoutes = require('./routes/auth')

const adminRoutes = require('./routes/admin/admin')
const adminAuthRoutes = require('./routes/admin/auth')

const keys = require('./keys')

const PORT = process.env.PORT || 3000

const app = express()

const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
    runtimeOptions: {
        allowProtoPropertiesByDefault: true,
        allowProtoMethodsByDefault: true
    }
})

const sequelizeSessionStore = new SessionStore({
    db: sequelize,
    checkExpirationInterval: 15 * 60 * 1000,
    expiration: 7 * 24 * 60 * 60 * 1000
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')

app.use(express.urlencoded({extended: true}))
app.use(express.static(path.join(__dirname, 'public')))
app.use(session({
    secret: keys.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store: sequelizeSessionStore,
    cookie : {
        sameSite: 'strict',
        maxAge: 7*24*60*60*1000
    }
}))
app.use(flash())
app.use(varMiddleware)

app.use('/', homeRoutes)
app.use('/', productsRoutes)
app.use('/product', productRoutes)
app.use('/favourites', favouritesRoutes)
app.use('/card', cardRoutes)
app.use('/order', orderRoutes)
app.use('/auth', authRoutes)

app.use('/admin', adminRoutes)
app.use('/admin/auth', adminAuthRoutes)


async function start() {
    try {
        await sequelize.sync()

        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`)
        })
    } catch (e) {
        console.log(e)
    }
}

start()

