const db = require('../utils/database')
const { QueryTypes } = require('sequelize')
const { getAll } = require('./product')

module.exports = {
    async getAll() {
        try {
            const staff = await db.query(`SELECT * FROM staff`, {type: QueryTypes.SELECT})
                                    .then(data => { return data })
                                    .catch(err => { throw err })

            return staff
        } catch (error) {
            throw error
        }
    },
    async findOne(login) {
        try {
            const staff = await db.query(`
                SELECT * FROM staff WHERE login_staff = '${login}'
            `, {type: QueryTypes.SELECT})
                .then(data => { return data[0] })
                .catch(err => { throw err })

            return staff
        } catch (error) {
            throw error
        }
    },
    async registration(obj) {
        try {
            const status = await db.query(`
                INSERT INTO staff (login_staff, password_hash, position_staff)
                VALUES ('${obj.login}', '${obj.password_hash}', '${obj.position}')
            `, {type: QueryTypes.INSERT})
                .then(data => { return data })
                .catch(err => { throw err })

            return status
        } catch (e) {
            throw e
        }
    },
    async deleteStaff(id) {
        const status = await db.query(`
            DELETE FROM staff WHERE id=${id};
            `, {type: QueryTypes.DELETE})
                .then(data => { return data })
                .catch(err => { throw err })
    }
}