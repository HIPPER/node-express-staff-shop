const db = require('../utils/database')
const { QueryTypes } = require('sequelize')
const { getProductsOnCategoryMale } = require('../controller/product.controller')

module.exports = {
    async getAll() {
        const products = await db.query("SELECT id, title, price, img FROM products", { type: QueryTypes.SELECT })
            .then(data => { return data })
            .catch(err => { throw err })
        
        return products
    },
    async getAllMale() {
        const products = await db.query("SELECT id, title, price, img FROM products WHERE gender='M'", { type: QueryTypes.SELECT })
            .then(data => { return data })
            .catch(err => { throw err })
        
        return products
    },
    async getAllFemale() {
        const products = await db.query("SELECT id, title, price, img FROM products WHERE gender='F'", { type: QueryTypes.SELECT })
            .then(data => { return data })
            .catch(err => { throw err })
        
        return products
    },
    async getProductsOnCategory(gender, category) {
        const products = await db.query(`SELECT id, title, price, img FROM products WHERE gender='${gender}' and category_name='${category}'`, { type: QueryTypes.SELECT })
            .then(data => { return data })
            .catch(err => { throw err })
        
        return products
    },
    async getProducts() {
        const products = await db.query("SELECT * FROM products", { type: QueryTypes.SELECT })
            .then(data => { return data })
            .catch(err => { throw err })
        
        return products
    },
    async getById(id) {
        const product = await db.query(`SELECT * FROM products WHERE id=${id}`, { type: QueryTypes.SELECT})
            .then(data => { return data[0] })
            .catch(err => { throw err })

        return product
    },
    async find(idx) {
        if (!idx) {
            return null
        }

        const products = await db.query(`SELECT id, title, price, img FROM products WHERE id IN (${idx});`, {type: QueryTypes.SELECT})
            .then(data => { return data })
            .catch(err => { throw err })

        return products
    },
    async addProduct(obj) {
        try {
            const status = await db.query(`
                INSERT INTO products (title, description, price, img, size, gender, category_name, in_stock)
                VALUES ('${obj.title}', '${obj.description}', ${obj.price}, '${obj.img}', '{${obj.sizes}}', '${obj.gender}', '${obj.category}', ${obj.in_stock})
            `, {type: QueryTypes.INSERT})
                .then(data => { return data })
                .catch(err => { throw err })
        } catch (e) {
            throw e
        }
    },
    async editProduct(obj) {
        const status = await db.query(`
            UPDATE products 
            SET title='${obj.title}', price=${obj.price}, img='${obj.img}', description='${obj.description}', size='{${obj.sizes}}', 
                gender='${obj.gender}', category_name='${obj.category}', in_stock=${obj.in_stock ? true : false}
            WHERE id = ${obj.id};
            `, {type: QueryTypes.UPDATE})
                .then(data => { return data })
                .catch(err => { throw err })
    },
    async deleteProduct(id) {
        const status = await db.query(`
            DELETE FROM products WHERE id=${id};
            `, {type: QueryTypes.DELETE})
                .then(data => { return data })
                .catch(err => { throw err })
    }
}