const db = require('../utils/database')
const { QueryTypes } = require('sequelize')

module.exports = {
    async getOrders() {
        const orders = await db.query(`
                SELECT orders.id, name_user, phone_user, order_city, order_number_branch, product_count, product_size, success,
                        p.title, p.price
                FROM orders
                INNER JOIN products p on p.id = orders.product_id
                ORDER BY orders.id DESC;
            `, { type: QueryTypes.SELECT})
                .then(data => { return data })
                .catch(err => { throw err })
        return orders
    },
    async doneOrder(id) {
        const status = await db.query(`
            UPDATE orders SET success = true WHERE id = ${id};
        `, {type: QueryTypes.UPDATE})
            .then(data => { return data })
            .catch(err => { throw err })
    },
    async createOrder(order) {
        let status
        for (var item in order.products) {
            status = await db.query(`
                INSERT INTO orders (name_user, phone_user, order_city, order_number_branch, product_id, product_count, product_size)
                VALUES ('${order.form.name}', '${order.form.tel}', '${order.form.city}', '${order.form.numberBranch}', ${order.products[item].id}, ${order.products[item].count}, '${order.products[item].size}');
            `, { type: QueryTypes.INSERT})
                .then(data => { return data })
                .catch(err => { throw err })
        }
        return status
    },
}