const db = require('../utils/database')
const { QueryTypes } = require('sequelize')

module.exports = {
    async getById(id) {
        const products = await db.query(`
            SELECT comment, full_name
            FROM reviews
            INNER JOIN products p on p.id = reviews.id_product
            INNER JOIN users u on u.id = reviews.id_user
            WHERE id_product = ${id}
            ORDER BY reviews.id DESC;
        `, { type: QueryTypes.SELECT })
            .then(data => { return data })
            .catch(err => { throw err })
        
        return products
    },
    async add(id_user, id_product, comment) {
        try {
            const status = await db.query(`
                INSERT INTO reviews (id_user, id_product, comment)
                VALUES (${id_user}, ${id_product}, '${comment}')
            `, {type: QueryTypes.INSERT})
                .then(data => { return data })
                .catch(err => { return err })
            return status
        } catch (e) {
            throw e
        }
    }
}