function addFavourites(obj) {
    const id = $("input", obj).attr("value")

    $.ajax({
        url: '/favourites',
        data: {"id": id},
        type: 'POST',
        success: function(response) {
            $("i", obj).removeClass().addClass("fa fa-heart")
        } 
    })
}

function removeFavourites(obj) {
    const id = $("input", obj).attr("value")

    $.ajax({
        url: '/favourites/delete',
        data: {"id": id},
        type: 'POST',
        success: function(response) {
            $("i", obj).removeClass().addClass("fa fa-heart-o")
        } 
    })
}

function changeQuantity(count, id) {
    $.ajax({
        url: '/card/quantity',
        data: {
            "id": id,
            "count": count
        },
        type: 'POST',
        success: function(price) {
            const $price = document.querySelector('#card .cart_order-info .cart_price')
            $price.innerHTML = `${price} грн`
        }
    })
}

$(".product-card__like").click(function() {
    if ($("i", this).attr('class') == "fa fa-heart-o") {
        addFavourites(this)
    } else {
        removeFavourites(this)
    }
})

$("#order-btn").click(function() {
    const id = $(this).attr("value")
    const title = $(this).data("title")
    const price = $(this).data("price")
    const img = $(this).data("img")
    const size = $("#btn_product-size .product__sizes--active").text()

    if (!size) {
        alert("Выберите размер одежды!")
    } else {
        $.ajax({
            url: '/card',
            data: {
                "id": id,
                "title": title,
                "price": price,
                "img": img,
                "size": size
            },
            type: 'POST',
            success: function(response) {
                alert('success')
            }
        })
    }
})

$(".cart_changeQuantity").click(function() {
    const val_btn = $(this).text()
    if (val_btn === '+') {
        let val = $(this).next().text()
        $(this).next().text(+val + 1)
        const quantity = $(this).next().text()
        const id = $(this).next().attr("value")
        changeQuantity(quantity, id)
    } else {
        let val = $(this).prev().text()
        if (val > 1) {
            $(this).prev().text(+val - 1)
            const quantity = $(this).prev().text()
            const id = $(this).prev().attr("value")
            changeQuantity(quantity, id)
        }
    }
})

$("#btn_product-size button").click(function() {
    $("#btn_product-size button").removeClass('product__sizes--active')
    $(this).addClass("product__sizes--active")
})

const $card = document.querySelector('#card')
if ($card) {
    $card.addEventListener('click', event => {
        if(event.target.classList.contains('cart_delete')) {
            const id = event.target.dataset.id

            $.ajax({
                url: '/card/remove/' + id,
                data: event.target.dataset,
                type: 'POST',
                success: function(card) {
                    if (card.length) {
                        const html = card.map(c => {
                            changeQuantity(c.count, c.id)

                            return `
                            <div class="cart_position">
                                <div class="cart_product">
                                    <div class="cart_product-img">
                                        <img src="${c.img}" alt="${c.title}">
                                    </div>
                                    <div class="cart_product-info">
                                        <div>${c.title}</div>
                                        <div class="cart_product-size">Размер: S</div>
                                    </div>
                                </div>
                                <div class="cart_order-data">
                                    <div class="cart_quantity">
                                        <button class="cart_changeQuantity">+</button>
                                        <span name="id" value="${c.id}">${c.count}</span>
                                        <button class="cart_changeQuantity">–</button>
                                    </div>
                                    <div class="cart_price">${c.price} грн</div>
                                    <button><i class="fa fa-trash-o fa-lg cart_delete" 
                                    data-id="${c.id}" data-title="${c.title}" data-img="${c.img}" data-price="${c.price}" 
                                    aria-hidden="true"></i></button>
                                </div>
                            </div>
                            `
                        }).join('')
                        $card.querySelector('.cart_positions').innerHTML = html
                    } else {
                        $card.innerHTML = '<h2 class="w-100 d-flex justify-content-center">В корзине нет товаров</h2>'
                    }
                }
            })
        }
    })
}