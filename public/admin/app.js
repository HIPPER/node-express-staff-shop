$('#addModal').on('shown.bs.modal', function () {
    $('#exampleModal').trigger('focus')
})

$(document).on("click", ".button-delete_product", function() {
    const id = $(this).data('id')
    $("#deleteModal .modal-footer #productId").val(id)
})

$(document).on("click", ".button-edit_product", function() {
    const productId = $(this).data('id')
    const productTitle = $(this).data('title')
    const productPrice = $(this).data('price')
    const productImg = $(this).data('img')
    const productDescription = $(this).data('description')
    //const productSizes = $(this).data('sizes')
    const productGender = $(this).data('gender')
    const productCategory = $(this).data('category_name')
    const productInstock = $(this).data('in_stock')

    $("#editModal #input-name").val(productTitle)
    $("#editModal #input-price").val(productPrice)
    $("#editModal #input-img").val(productImg)
    $("#editModal #input-description").val(productDescription)
    $("#editModal #productId").val(productId)

    if (productGender === 'M') {
        $("#gender_m").prop("checked", true)
    } else if (productGender === 'F') {
        $("#gender_f").prop("checked", true)
    }

    $("#category").val(productCategory)

    if (productInstock) {
        $("#input-in_stock").prop("checked", true)
    } else {
        $("#input-in_stock").prop("checked", false)
    }
})

$(".btn-done_order").click(function() {
    const id = $(this).data('id')
    const btn = $(this)[0]

    $.ajax({
        url: '/admin/order',
        data: {
            "id": id
        },
        type: 'POST',
        success: function(response) {
            btn.outerHTML = `<button type="button" class="btn btn-secondary" disabled>Завершено</button>`
        }
    })
})